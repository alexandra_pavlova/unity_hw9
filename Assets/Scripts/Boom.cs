using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boom : MonoBehaviour
{
    public float radius;
    public float timeToExplosion;
    public float power;
    
    void Start()
    {
        
    }

    void Update()
    {
        timeToExplosion -= Time.deltaTime;
        if (timeToExplosion <= 0)
        {
            BoomOn();
        }
    }

    void BoomOn()
    {
        print("Boom!");
        Rigidbody[] Blocks = FindObjectsOfType<Rigidbody>();
        foreach (Rigidbody B in Blocks)
        {
            if (Vector3.Distance(transform.position, B.transform.position) < radius)
            {
                Vector3 direction = B.transform.position - transform.position;
                B.AddForce(direction.normalized * power *
                           (radius - Vector3.Distance(transform.position, B.transform.position)));
            }
        }
        timeToExplosion = 3;
    }
}
