using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2Script : MonoBehaviour
{
    public Vector3 startImpulse;
    void Start()
    {
        GetComponent<Rigidbody>().AddForce(startImpulse, ForceMode.Impulse);
    }

    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody)
        {
            Vector3 punchVector = other.transform.position - transform.position;
            //GetComponent<Rigidbody>().AddForce(punchVector.normalized * 3, ForceMode.Impulse);
            other.rigidbody.AddForce(punchVector.normalized * 1, ForceMode.Impulse);
        }
    }
}
