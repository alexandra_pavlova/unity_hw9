using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupermanScript : MonoBehaviour
{
    public float power;
    private Vector3 punchVector; 
    
    void Start()
    {
        GetComponent<Rigidbody>().AddForce(1,0,0, ForceMode.Impulse);
    }
    
    void Update()
    {
        
    }
    

    private void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody)
        {
            punchVector = other.transform.position - transform.position;
            //GetComponent<Rigidbody>().AddForce(punchVector.normalized * power, ForceMode.Impulse);
            other.rigidbody.AddForce(punchVector.normalized * power, ForceMode.Impulse);
        }
    }
}
